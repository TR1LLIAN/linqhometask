﻿using CollectionsLinq.Services;
using System;
using Task = System.Threading.Tasks.Task;

namespace CollectionsLinq
{
    class Program
    {


        static async Task Main(string[] args)
        {
            Console.ResetColor();
            Menu menu = new Menu();
            await menu.ShowMenu();

        }
    }
}
