﻿using CollectionsLinq.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLinq.Models
{
    public class TeamModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> TeamPerformes { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
