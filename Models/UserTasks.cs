﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLinq.Models
{
    public class UserTasks
    {
        public Entities.User User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
