﻿using CollectionsLinq.Entities;
using System;

namespace CollectionsLinq.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PerformerId { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public User Performer { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
