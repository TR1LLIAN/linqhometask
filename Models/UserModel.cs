﻿using CollectionsLinq.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLinq.Models
{
    public class UserModel
    {
        public User User { get; set; }
        public List<TaskModel>? UnFinishedTasks { get; set; }
        public Project ? LastProject { get; set; }
        public TaskModel? LongestTask { get; set; }
    }
}
