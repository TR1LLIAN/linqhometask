﻿using CollectionsLinq.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLinq.Models
{
    public class ProjectInfo
    {
        public Project Project { get; set; }
        public TaskModel LongestTaskDescription { get; set; }
        public TaskModel ShortestTaskByName { get; set; }
        public int? UsersCount { get; set; }

    }
}
