﻿using CollectionsLinq.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLinq.Models
{
    public class ProjectModel
    {
        public Project Project { get; set; }
        public User Author { get; set; }
        public List<Models.TaskModel> Tasks { get; set; }
        public TeamModel Team { get; set; }

    }
}
