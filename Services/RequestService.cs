﻿using CollectionsLinq.Entities;
using CollectionsLinq.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollectionsLinq.Services
{
    public class RequestService
    {
        public static async Task<List<ProjectModel>> AggregateEntitiesAsync()
        {
            List<Entities.Project> projects = await DataAccessService.GetProjectsAsync();
            List<Entities.User> users = await DataAccessService.GetUsersAsync();
            List<Entities.Team> teams = await DataAccessService.GetTeamsAsync();
            List<Entities.Task> tasks = await DataAccessService.GetTasksAsync();

            List<Models.TaskModel> taskModels = tasks.Join(users,
                t => t.PerformerId,
                u => u.Id, (t, u) => new TaskModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                    FinishedAt = t.FinishedAt,
                    CreatedAt = t.CreatedAt,
                    Description = t.Description,
                    PerformerId = t.PerformerId,
                    ProjectId = t.ProjectId,
                    Performer = u
                }).ToList();

            List<Models.TeamModel> teamModels = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId, (t, u) => new TeamModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                    CreatedAt = t.CreatedAt,
                    TeamPerformes = u.Where(u => u.TeamId == t.Id).ToList()
                }).ToList();



            var result = projects.Join(users,
                p => p.AuthorId,
                u => u.Id,
                (p, u) => new ProjectModel
                {
                    Project = p,
                    Author = u

                }).Join(teamModels,
                p => p.Project.TeamId,
                t => t.Id,
                (p, t) => new ProjectModel
                {
                    Project = p.Project,
                    Author = p.Author,
                    Team = t
                }).GroupJoin(taskModels,
                p => p.Project.Id,
                t => t.ProjectId,
                (p, t) => new ProjectModel
                {
                    Project = p.Project,
                    Team = p.Team,
                    Author = p.Author,
                    Tasks = t.Where(t => t.ProjectId == p.Project.Id)
                    .Select(x => new TaskModel()
                    {
                        Name = x.Name,
                        Id = x.Id,
                        Description = x.Description,
                        PerformerId = x.PerformerId,
                        CreatedAt = x.CreatedAt,
                        ProjectId = x.ProjectId,
                        FinishedAt = x.FinishedAt,
                        Performer = x.Performer
                    }).ToList()
                }).ToList();

            return result;
        }



        public static async Task<List<UserTasks>> GetUsers()
        {
            var data = await AggregateEntitiesAsync();

            return data.SelectMany(x => x.Tasks)
                .Select(x => x.Performer).Distinct().OrderBy(x => x.FirstName)
                .GroupJoin(data.SelectMany(x => x.Tasks), p => p.Id, t => t.PerformerId, (p, t) => new UserTasks
                {
                    User = p,
                    Tasks = t.Where(t => t.PerformerId == p.Id).OrderByDescending(t => t.Name.Length).ToList()
                }).ToList();
        }

        public static async Task<List<TeamModel>> GetTeams()
        {
            var data = await AggregateEntitiesAsync();
            return data.Select(p => p.Team)
                .Where(x => x.TeamPerformes.All(x => (DateTime.Now.Year - x.BirthDay.Year) >= 10))
                .Select(p => new TeamModel
                {
                    Name = p.Name,
                    Id = p.Id,
                    CreatedAt = p.CreatedAt,
                    TeamPerformes = p.TeamPerformes.Where(x => (DateTime.Now.Year - x.BirthDay.Year) >= 10).OrderByDescending(x => x.RegisteredAt).ToList()
                }).ToList();
        }


        public static async Task<List<TaskModel>> GetUserTasksAsync(int id)
        {
            var data = await AggregateEntitiesAsync();

            return data.Select(p => new ProjectModel
            {
                Project = p.Project,
                Tasks = p.Tasks.Where(t => t.PerformerId == id && t.Name.Length < 45).ToList()
            })
                .Where(x => x.Tasks.Count > 0)
                .SelectMany(x => x.Tasks).ToList();
        }

        public static async Task<UserModel> GetUserProject(int id)
        {
            var data = await AggregateEntitiesAsync();

            return data.OrderByDescending(x => x.Project.CreatedAt).Where(x => x.Author.Id == id)
                .Select(x => new UserModel 
                {
                    User = x.Author,
                    LastProject = x.Project,
                    LongestTask = x.Tasks.OrderByDescending(x => (x?.FinishedAt - x.FinishedAt)).FirstOrDefault(),
                    UnFinishedTasks = x.Tasks.Where(x => x.FinishedAt == null).ToList()
                }).FirstOrDefault();
        }

        public static async Task<Dictionary<Project, int>> GetNumberOfTaskPerUserPerProject(int id)
        {
            var data = await AggregateEntitiesAsync();

            return data.Select(p => new ProjectModel
            {
                Project = p.Project,
                Tasks = p.Tasks.Where(t => t.PerformerId == id).ToList()
            })
            .ToDictionary(x => x.Project, x => x.Tasks.Count)
            .Where(x => x.Value > 0).ToDictionary(x => x.Key, x => x.Value);
        }

        public static async Task<List<ProjectInfo>> GetProjects()
        {
            List<ProjectModel> data = await AggregateEntitiesAsync();
            return data.Select(x => new ProjectInfo
            {
                Project = x.Project,
                LongestTaskDescription = x.Tasks.OrderByDescending(x => x.Description.Length).FirstOrDefault(),
                ShortestTaskByName = x.Tasks.OrderBy(x => x.Name).FirstOrDefault(),
                UsersCount = ((x.Project.Name.Length > 20 || x.Tasks.Count < 3) ? x.Team.TeamPerformes.Count : null)
            }).ToList();

        }

        public static async Task<List<TaskModel>> GetUserFinishedTasksAsync(int id)
        {
            var data = await AggregateEntitiesAsync();
            DateTime dateTime = new DateTime(2021);

            return data.Select(p => new ProjectModel
            {
                Project = p.Project,
                Tasks = p.Tasks.Where(t => t.PerformerId == id && t.FinishedAt > dateTime).ToList()
            })
               .Where(x => x.Tasks.Count > 0)
               .SelectMany(x => x.Tasks.ToList()).ToList();

        }


    }


}
