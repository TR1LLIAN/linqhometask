﻿using CollectionsLinq.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CollectionsLinq.Services
{
    public static class DataAccessService
    {
        static readonly HttpClient client = new HttpClient();
        const string Alias = "https://bsa21.azurewebsites.net/api/";

        public static async Task<List<User>> GetUsersAsync()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + "Users");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<User> users = JsonConvert.DeserializeObject<List<User>>(temp);
            return users;
        }

        public static async Task<List<Project>> GetProjectsAsync()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + "Projects");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(temp);
            return projects; 
        }

        public static async Task<List<Entities.Task>> GetTasksAsync()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + "Tasks");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<Entities.Task> tasks = JsonConvert.DeserializeObject<List<Entities.Task>>(temp);
            return tasks;
        }

        public static async Task<List<Team>> GetTeamsAsync()
        {
            HttpResponseMessage response = await client.GetAsync(Alias + "Teams");
            response.EnsureSuccessStatusCode();
            string temp = await response.Content.ReadAsStringAsync();
            List<Team> teams = JsonConvert.DeserializeObject<List<Team>>(temp);
            return teams;
        }


    }
}
